<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Thyssenkrup</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid">
    <div class="row p-3">
        <div class="col-12 col-md-4 p-2">
            <img src="img/logo.png" class="img-fluid" alt=""/> 
        </div>
        <div class="col-12 col-md-4 p-2 text-center">
            <img src="img/title.png" class="img-fluid" alt=""/> 
        </div>
        
    </div>
    <div class="row mt-2">
      <div class="col-12 col-md-6 col-lg-4 offset-md-3 offset-lg-4">
            <div class="login">
                <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Name" name="name" id="name" required>
                  </div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="8 ID" name="8id" id="8id" required>
                  </div>
                  <div class="input-group">
                    <input type="email" class="form-control" placeholder="Email ID" name="email" id="email" required>
                  </div>
                  <div class="form-group mt-4 text-center">
                    <input type="image" src="img/btn-login.png" value="Submit">
                  </div>
                </form>
            </div>
        
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script>
</body>
</html>