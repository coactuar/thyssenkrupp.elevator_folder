<?php
require_once "../config.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {
    
    $action = $_POST['action'];
    
    switch($action) {
        
        
        case 'getusers':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
            $today=date('Y/m/d H:i:s');
            $sql = "SELECT COUNT(id) as count FROM tbl_users where eventname='$event_name' and logout_date > '$today'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $loggedin = $row['count'];
        
            
            $sql = "SELECT COUNT(id) as count FROM tbl_users where eventname='$event_name'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Users: <?php echo $total_records; ?>
                </div>
                <div class="col-6">
                    Currently Logged In: <div id="logged-in"><?php echo $loggedin; ?></div>
                </div>
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped table-dark">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Name</th>
                          <th>Email ID</th>
                          <th>8 ID</th>
                          <th>Registered On</th>
                          <th>Last Login Time</th>
                          <th>Last Logout Time</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_users where eventname='$event_name' order by logout_status desc, login_date desc LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        $loggedin = 0;
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['user_name']; ?></td>
                            <td><?php echo $data['user_email']; ?></td>
                            <td><?php echo $data['user_8id']; ?></td>
                            <td><?php 
                                if($data['joining_date'] != ''){
                                    $date=date_create($data['joining_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                            <td><?php 
                                if($data['login_date'] != ''){
                                    $date=date_create($data['login_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                            <td><?php
                                $today=date("Y/m/d H:i:s");
        
                                $dateTimestamp1 = strtotime($data['logout_date']);
                                $dateTimestamp2 = strtotime($today);
                                //echo $row[5];
                                if ($dateTimestamp1 > $dateTimestamp2)
                                {
                                  echo "Logged In";
                                  //$loggedin += 1; 
                                }
                                else
                                { 
                                  if($data['logout_date'] != ''){
                                      $date=date_create($data['logout_date']);
                                      echo date_format($date,"M d, H:i a"); 
                                  }
                                  else{
                                      echo '-';
                                  }
                                  if($data['logout_status']=='1')
                                  {
                                    $ls="UPDATE tbl_users set logout_status='0' where id='".$data['id']."' and eventname='$event_name'";
                                    $lsres = mysqli_query($link, $ls) or die(mysqli_error($link));
                                  }
                                }
                                ?>
                            </td>
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
        

        
        
    }
    
}


?>