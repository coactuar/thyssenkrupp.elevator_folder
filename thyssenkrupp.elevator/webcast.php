<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_email"]))
	{
		header("location: ./");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $user_email=$_SESSION["user_email"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where user_email='$user_email'  and eventname='$event_name'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_email"]);
            
            header("location: .");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Thyssenkrupp</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid">
     <div class="row p-3">
        <div class="col-12 col-md-4 p-2">
            <img src="img/logo.png" class="img-fluid" alt=""/> 
        </div>
        <div class="col-12 col-md-4 p-2 text-center">
            <img src="img/title.png" class="img-fluid" alt=""/> 
        </div>
        
    </div>
    <div class="row p-2">
        <div class="col-12 p-5 text-right" style="color:grey;">
			
          Hello, <?php echo $_SESSION['user_name']; ?>! <a class="btn btn-sm btn-light" href="?action=logout">Logout</a>
		
        </div>
    </div>
    <div class="row p-3">
      <div class="col-12 col-md-6 offset-md-3">
            <!--<div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" id="webcast" src="video.php" allowfullscreen></iframe>
            </div>-->
			<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://vimeo.com/event/505841/embed/35eac4a9d9" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>
        </div>
        
    </div>
    
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
/*
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
         }
});
}
setInterval(function(){ update(); }, 30000);
*/
</script>



</body>
</html>